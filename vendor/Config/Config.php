<?php

namespace Config;

class Config
{
    const CONFIG_PATH = __DIR__ . '/../../config';

    public static function __callStatic($name, $arguments)
    {
        // check file exist
        $configFilePath = static::CONFIG_PATH . "/{$name}.php";

        if (!file_exists($configFilePath)) {
            throw new \Exception('Config not found');
        }

        $config = include_once $configFilePath;

        if (isset($config['default'])) {
            $default = $config['default'];

            if (isset($config[$default])) {
                return $config[$default];
            }
        } else {
            return $config;
        }

        throw new \Exception('Config not found');
    }
}