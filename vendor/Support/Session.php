<?php

namespace Support;

class Session
{
    public static function isLoggedIn()
    {
        session_start();

        return isset($_SESSION['logged_in']);
    }

    public static function user()
    {
        if (static::isLoggedIn()) {
            return [
                'id' => $_SESSION['logged_in_id'],
                'email' => $_SESSION['email'],
            ];
        }

        return null;
    }
}