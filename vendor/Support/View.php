<?php

namespace Support;

class View
{
    public static function make(string $template, array $arguments = [])
    {
        extract($arguments);
        $templatePath = __DIR__ . '/../../resources/views/' . $template . '.php';
        return require_once $templatePath;
    }
}