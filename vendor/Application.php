<?php

use Http\Request;

class Application
{
    protected $controllerPrefix = 'app\\Controllers\\';
    protected $middlewarePrefix = 'app\\Middleware\\';
    protected $request;
    protected $routes;
    protected $routeVariables = [];

    public function __construct()
    {
        $this->request = new Request();
        $this->routes = \Http\Route::routes();
        $this->proceedRequest();
    }

    protected function proceedRequest()
    {
        $route = $this->mapRequestToRoute();

        if ($route) {
            $controllerClass = $this->controllerPrefix . $route['controller'];

            // check via middleware to ensure request is valid
            if ($route['middleware']) {
                $middlewareClass = $this->middlewarePrefix . $route['middleware'];
                call_user_func([$middlewareClass, 'handle']);
            }

            if ($route['method']) {
                return call_user_func_array([$controllerClass, $route['method']], $this->routeVariables);
            }
        }

        if ($this->request->getContentType() === 'application/json') {
            return \Http\Response::json(['message' => 'Method not allow'], 400);
        }

        return \Support\View::make('404');
    }

    protected function mapRequestToRoute()
    {
        $requestUri = $this->request->getRequestUri();
        $requestUriToArray = explode('/', $requestUri);
        $requestMethod = $this->request->getRequestMethod();
        $routeMapRequestMethod = $this->routes[$requestMethod];

        foreach ($routeMapRequestMethod as $route) {
            $routeUriAsArray = explode('/', $route['uri']);
            $mapped = true;

            // check if length is match
            if (count($requestUriToArray) === count($routeUriAsArray)) {
                // check each part if match
                foreach ($requestUriToArray as $key => $value) {
                    if ($value === $routeUriAsArray[$key]) {
                        continue;
                    }

                    // check if route part is a route variable
                    if (strpos($routeUriAsArray[$key], '{') === 0) {
                        $this->routeVariables[] = $value;
                        continue;
                    }

                    $mapped = false;
                    break;
                }
            } else {
                // length not match
                $mapped = false;
            }

            if ($mapped) {
                return $route;
            }
        }

        return null;
    }
}