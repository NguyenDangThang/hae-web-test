<?php

namespace Database;

use Config\Config;
use Database\Builder;

class DB
{
    protected $builder;

    public function __construct()
    {
        $this->builder = new Builder;
    }

    public function table($table)
    {
        $this->builder->setPDO(static::pdo());
        $this->builder->setTable($table);

        return $this->builder;
    }

    public static function pdo()
    {
        return static::establishConnection();
    }

    protected static function establishConnection()
    {
        // establish connection to database
        // load config database

        try {
            $config = Config::database();
            $driver = $config['driver'];
            $host = $config['host'];
            $port = $config['port'];
            $dbname = $config['database'];
            $charset = $config['charset'];
            $collation = $config['collation'];
            $username = $config['username'];
            $password = $config['password'];


            $dsn = "$driver:host=$host;port=$port;dbname=$dbname;charset=$charset;collaction=$collation";
            $options = [
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES   => false,
            ];

            $pdo = new \PDO($dsn, $username, $password, $options);

            return $pdo;
        } catch (\Exception $exception) {
            throw new \Exception('Cannot establish connection to database');
        }
    }
}