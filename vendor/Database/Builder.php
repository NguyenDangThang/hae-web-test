<?php

namespace Database;

class Builder
{
    protected $pdo;
    protected $table;

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function setPDO(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

//    public function executeRawQuery($query, $bindingArguments = [])
//    {
//        $this->pdo->prepare($query);
//        return $this->pdo->execute($bindingArguments);
//    }

    public function insert(array $values)
    {
        return $this->prepareInsert($values)->execute(array_values($values));
    }

    public function update(array $valuesToUpdate, $conditions = [])
    {
        return $this->prepareUpdate($valuesToUpdate, $conditions)
            ->execute(array_merge(array_values($valuesToUpdate), array_values($conditions)));
    }

    public function destroy(array $conditions)
    {
        return $this->prepareDelete($conditions)->execute(array_values($conditions));
    }

    protected function prepareInsert(array $valuesToInsert)
    {
        // get list columns and values
        $columns = [];
        $values = [];
        $valuesHint = [];

        foreach ($valuesToInsert as $column => $value) {
            $columns[] = $column;
            $values[] = $value;
            $valuesHint[] = '?';
        }

        $columnsAsString = implode(',', $columns);
        $valuesHintAsString = implode(',', $valuesHint);

        return $this->pdo->prepare("INSERT INTO {$this->table} ($columnsAsString) VALUES ($valuesHintAsString)");
    }

    protected function prepareUpdate(array $valuesToUpdate, $conditions = [])
    {
        // get list columns and values
        $columnSet = [];
        $columnSetAsString = '';
        $conditionSet = [];
        $conditionSetAsString = '';

        foreach ($valuesToUpdate as $column => $value) {
            $columnSet[] = $column . ' = ?';
        }

        foreach ($conditions as $condition => $value) {
            $conditionSet[] = $condition . ' = ?';
        }

        $columnSetAsString = implode(',', $columnSet);
        $conditionSetAsString = implode(' AND ', $conditionSet);

        $prepareQuery = "UPDATE {$this->table} SET $columnSetAsString ";

        if ($conditionSetAsString) {
            $prepareQuery .= "WHERE $conditionSetAsString";
        }

        return $this->pdo->prepare($prepareQuery);
    }

    protected function prepareDelete(array $conditions)
    {
        $conditionSet = [];
        $conditionSetAsString = '';

        foreach ($conditions as $condition => $value) {
            $conditionSet[] = $condition . ' = ?';
        }

        $conditionSetAsString = implode(' AND ', $conditionSet);

        return $this->pdo->prepare("DELETE FROM {$this->table} WHERE $conditionSetAsString");
    }
}