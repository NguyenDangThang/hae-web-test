<?php

namespace Http;

class Request
{
    protected $request_method;
    protected $request_uri;
    protected $requestParams;
    protected $content_type;

    public function __construct()
    {
        // base on $_SERVER
        $this->request_method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->content_type = isset($_SERVER['CONTENT_TYPE']) ? strtolower($_SERVER['CONTENT_TYPE']) : '';
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $this->request_uri = $uri[0];

        $this->loadAllRequestParams();
    }

    public function getRequestMethod()
    {
        $requestMethod = strtolower($this->request_method);
        $methodParam = $this->get('_method');

        if (isset($methodParam) && strtolower($methodParam) === 'put') {
            return 'put';
        }

        return $requestMethod;
    }

    public function getContentType()
    {
        return strtolower($this->content_type);
    }

    public function getRequestUri()
    {
        return strtolower(ltrim($this->request_uri, '/'));
    }

    public function get($param)
    {
        return isset($this->requestParams[$param]) ? $this->requestParams[$param] : null;
    }

    public function all()
    {
        return $this->requestParams;
    }

    /**
     * Load all request params include url params and post params
     */
    protected function loadAllRequestParams()
    {
        $uri = explode('?', $_SERVER['REQUEST_URI']);

        if (isset($uri[1])) {
            $params = explode('&', $uri[1]);

            foreach ($params as $param) {
                $paramAsArray = explode('=', $param);
                $this->requestParams[$paramAsArray[0]] = (isset($paramAsArray[1]) ? $paramAsArray[1] : null);
            }
        } else {
            $this->requestParams = [];
        }

        if ($this->request_method === 'post') {
            foreach ($_POST as $key => $value) {
                $this->requestParams[$key] = $value;
            }
        }
    }
}