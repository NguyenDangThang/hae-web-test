<?php

namespace Http;

class Route
{
    protected static $prefix = '';
    protected static $routes = [
        'get' => [],
        'post' => [],
        'put' => [],
        'delete' => [],
    ];

    public static function routes()
    {
        return static::$routes;
    }

    public static function prefix(string $prefix = '')
    {
        // reset prefix to standard (allow user config prefix as '/' or '')
        $prefix = rtrim('/');

        if ($prefix) {
            $prefix .= '/';
        }

        return $prefix;
    }

    public static function get(string $uri, string $handle, string $middleware = '')
    {
        return static::addRoute('get', $uri, $handle, $middleware);
    }

    public static function post(string $uri, string $handle, string $middleware = '')
    {
        return static::addRoute('post', $uri, $handle, $middleware);
    }

    public static function put(string $uri, string $handle, string $middleware = '')
    {
        return static::addRoute('put', $uri, $handle, $middleware);
    }

    public static function delete(string $uri, string $handle, string $middleware = '')
    {
        return static::addRoute('delete', $uri, $handle, $middleware);
    }

    protected static function addRoute(string $httpMethod, string $uri, string $handle, string $middleware = '')
    {
        $uri = ltrim($uri, '/');
        $handleAsArray = explode('@', $handle);
        $controller = $handleAsArray[0];
        $method = isset($handleAsArray[1]) ? $handleAsArray[1] : null;

        static::$routes[$httpMethod][] = [
            'uri' => static::$prefix . $uri,
            'controller' => $controller,
            'method' => $method,
            'middleware' => $middleware
        ];
    }
}