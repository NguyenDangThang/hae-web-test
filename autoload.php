<?php

require_once 'vendor/Application.php';
require_once 'vendor/Http/Request.php';
require_once 'vendor/Http/Response.php';
require_once 'vendor/Http/Route.php';
require_once 'vendor/Support/View.php';
require_once 'vendor/Support/Session.php';
require_once 'vendor/Database/DB.php';
require_once 'vendor/Database/Builder.php';
require_once 'vendor/Config/Config.php';

require_once 'routes/web.php';
require_once 'routes/api.php';

require_once 'app/Controllers/GuestbookController.php';
require_once 'app/Controllers/LoginController.php';

require_once 'app/Controllers/Api/GuestbookController.php';

require_once 'app/Middleware/GuestMiddleware.php';
require_once 'app/Middleware/AdminMiddleware.php';