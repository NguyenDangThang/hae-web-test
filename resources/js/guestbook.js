$(document).ready(function () {
    $('#feedbackModal').on('hidden.bs.modal', function (event) {
        // refresh form
        $(this).find('#fullname').val('');
        $(this).find('#message').val('');

        $(this).find('form .wrap-input').show();
        $(this).find('.success-submit').hide();

        $(this).find('.action-before-submit').show();
        $(this).find('.action-submitted').hide();

        $(this).find('.error').empty();
        $(this).find('.error').hide();
    });

    $('#feedbackModalEdit').on('hidden.bs.modal', function (event) {
        $(this).find('.error').empty();
        $(this).find('.error').hide();

        $(this).find('.success').hide();
        $('.edit-success .toast').toast('hide');
    });
    
    $('.refresh-page').on('click', function () {
        document.location.reload();
    })


    $('#feedbackModal').on('submit', function ($event) {
        $event.preventDefault();

        var self = this;
        var fullName = $(this).find('#fullname').val();
        var message = $(this).find('#message').val();
        var requestPayload = 'full_name=' + fullName + '&message=' + message;

        $('#feedbackModal .modal-footer button').attr("disabled", true);
        // submit data
        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                // request.setRequestHeader("Content-Type", 'application/json');
            },
            data: {'full_name': fullName, 'message': message},
            url: '/api/message',
        }).done(function (response) {
            $(self).find('form .wrap-input').hide();
            $(self).find('.success-submit').show();

            $(self).find('.action-before-submit').hide();
            $(self).find('.action-submitted').show();

            $(self).find('.error').empty();
            $(self).find('.error').hide();
            $('#feedbackModal .modal-footer button').removeAttr("disabled");
        }).fail(function(xhr) {
            // error handling
            var error = xhr.responseJSON;
            var errorMessages = error['message'];
            var errorElement = '';

            if (Array.isArray(errorMessages)) {
                for (var i = 0; i < errorMessages.length; i++) {
                    errorElement += ('<p> - ' + errorMessages[i] + '</p>');
                }
            } else {
                errorElement += ('<p>' + errorMessages + '</p>');
            }

            $(self).find('.error').empty();
            $(self).find('.error').append(errorElement);
            $(self).find('.error').show();
            $('#feedbackModal .modal-footer button').removeAttr("disabled");
        });
    })


    $('#feedbackModalEdit').on('submit', function ($event) {
        $event.preventDefault();

        var self = this;
        var fullName = $(this).find('#fullname_edit').val();
        var message = $(this).find('#message_edit').val();
        var editId = $(this).find('#edit_id').val();

        $('#feedbackModalEdit .modal-footer button').attr("disabled", true);

        $('.edit-success .toast').toast('hide');
        // submit data
        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                // request.setRequestHeader("Content-Type", 'application/json');
            },
            data: {'full_name': fullName, 'message': message, '_method': 'put'},
            url: '/api/message/' + editId,
        }).done(function (response) {
            $('#feedbackModalEdit .modal-footer button').removeAttr("disabled");
            $('#feedbackModalEdit .success').show();
            $(self).find('.error').empty();
            $(self).find('.error').hide();

            $('.message[data-id=' + editId + '] p').text(message);
            $('.message[data-id=' + editId + '] .author').text(fullName);
            setTimeout(function () {
                $('.edit-success .toast').toast('show');
            }, 300);
        }).fail(function(xhr) {
            if (xhr.status == 401) {
                window.location.href = '/admin-login';
                return false;
            }
            // error handling
            var error = xhr.responseJSON;
            var errorMessages = error['message'];
            var errorElement = '';

            if (Array.isArray(errorMessages)) {
                for (var i = 0; i < errorMessages.length; i++) {
                    errorElement += ('<p> - ' + errorMessages[i] + '</p>');
                }
            } else {
                errorElement += ('<p>' + errorMessages + '</p>');
            }

            $(self).find('.error').empty();
            $(self).find('.error').append(errorElement);
            $(self).find('.error').show();

            $('#feedbackModalEdit .modal-footer button').removeAttr("disabled");
        });
    })

    $('#confirm-delete').on('submit', function ($event) {
        $event.preventDefault();

        var deleteId = $(this).find('#delete_id').val();

        $('#confirm-delete .modal-footer button').attr("disabled", true);

        $('.delete-success .toast').toast('hide');

        $.ajax({
            type: "DELETE",
            beforeSend: function(request) {
                // request.setRequestHeader("Content-Type", 'application/json');
            },
            url: '/api/message/' + deleteId,
        }).done(function (response) {
            window.sessionStorage.setItem('delete_success', 1);
            window.location.reload();
        }).fail(function(xhr) {
            if (xhr.status == 401) {
                window.location.href = '/admin-login';
                return false;
            }

            $('.delete-failed .toast').toast('show');
            $('#confirm-delete .modal-footer button').removeAttr("disabled");
        });
    })
});


function openEditModal(elem, id) {
    if (typeof $ === 'undefined') {
        console.log('jQuery not found');
    }

    // get data to parse to edit modal
    // find closet message element
    var $message = $(elem).closest('.message');
    var fullName = $($message).find('.author').text();
    var message = $($message).find('p').text();
    var editId = $(elem).attr('data-id');

    // open edit modal
    $('#feedbackModalEdit form #edit_id').val(editId);
    $('#feedbackModalEdit form #fullname_edit').val(fullName);
    $('#feedbackModalEdit form #message_edit').val(message);

    $('#feedbackModalEdit').modal()
}

function openConfirmDeleteModal(elem, id) {
    if (typeof $ === 'undefined') {
        console.log('jQuery not found');
    }

    // get data to parse to edit modal
    // find closet message element
    var deleteId = $(elem).attr('data-id');

    // open edit modal
    $('#confirm-delete form #delete_id').val(deleteId);

    $('#confirm-delete').modal()
}