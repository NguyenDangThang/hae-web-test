<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Guestbook</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="../../resources/css/style.css">
</head>
<body class="guestbook">
<div class="container page-detail">
    <div class="row">
        <div class="col-lg-3 left-aside">
            <div class="text-center">
                <a href="javascript:void(0)" class="logo">
                    <img src="../../resources/images/logo-hae-group.png" alt="logo">
                </a>
                <h1 class="page-title">Guestbook</h1>
            </div>
            <div class="welcome">
                <p>Feel free to leave us a short message to tell us what you think to our services</p>
            </div>
            <button  data-toggle="modal" data-target="#feedbackModal" class="btn btn-danger add-feedback">
                Post a message
            </button>
            <div class="user">
                <?php
                    if (isset($loggedUser)) {
                ?>
                        <h6>Welcome <?php echo $loggedUser['email']; ?></h6>
                        <div>
                            <a href="/logout">Logout</a>
                        </div>
                <?php
                    } else {
                        echo "<a href='/admin-login'>Admin login</a>";
                    }
                ?>
            </div>
        </div>
        <div class="col-lg-9 content">

            <div class="row clearfix">
                <?php
                    foreach($messages as $message) {
                    ?>
                        <div class="col-md-6 message" data-id="<?php echo $message['id'] ?>">
                            <p><?php echo $message['message']; ?></p>
                            <div class="message-bottom">
                                <div class="author"><?php echo $message['full_name']; ?></div>
                                <span class="datetime">
                                    <?php
                                        $createdAt = $message['created_at'];
                                        $dateFormatted = date('dS M, Y', strtotime($createdAt));
                                        $timeFormatted = date('h:ia', strtotime($createdAt));
                                        $createdAtFormatted = $dateFormatted . ' at ' . $timeFormatted;
                                        echo $createdAtFormatted;
                                    ?>
                                </span>
                                <?php
                                if (isset($loggedUser)) {
                                    ?>
                                    <div class="action">
                                        <a href="javascript:void(0)" class="action-btn" data-id="<?php echo $message['id']; ?>"
                                            onclick="openEditModal(this, <?php echo $message['id']; ?>)"
                                        >
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="action-btn" data-id="<?php echo $message['id']; ?>"
                                            onclick="openConfirmDeleteModal(this, <?php echo $message['id']; ?>)">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    }
                ?>
            </div>
            <?php
                if (count($messages) === 0) {
//                    echo "<h3>No message on this page</h3>";
                }
            ?>
            <?php
                if (count($messages) > 0) {
                    ?>
                    <div class="row clearfix wrap-pagination">
                        <div class="pagination">
                            <!-- previous button -->
                            <?php
                            $currentPage = $pagination['current_page'];

                            if ($pagination['previous_page']) {
                                $previousPage = '?page=' . $pagination['previous_page'];
                                $disablePreviousPageBtn = false;
                            } else {
                                $previousPage = 'javascript:void(0)';
                                $disablePreviousPageBtn = true;
                            }
                            ?>
                            <a href="<?php echo $previousPage; ?>" class="<?php if ($disablePreviousPageBtn) {
                                echo 'disabled';
                            } ?>" style="line-height: 20px;">
                                <i class="fas fa-angle-left"></i>
                            </a>

                            <?php
                            $hasMoreTagLink = false;

                            for ($page = 1; $page <= $pagination['total_page']; $page++) {
                                $totalPage = $pagination['total_page'];

//                                if ($totalPage > 3) {
//                                    if ($page > $currentPage + 1 && $page < $totalPage - 1) {
//                                        if ($hasMoreTagLink) {
//                                            continue;
//                                        }
//                                        ?>
<!--                                        <a href="javascript:void(0)" class="more">...</a>-->
<!--                                        --><?php
//                                        $hasMoreTagLink = true;
//                                        continue;
//                                    }
//                                }
                                ?>
                                <a href="<?php echo($page == $pagination['current_page'] ? 'javascript:void(0)' : '?page=' . $page); ?>"
                                   class="<?php
                                   if ($page == $pagination['current_page']) {
                                       echo 'active';
                                   }
                                   ?> normal"><?php echo $page; ?></a>
                                <?php
                            }
                            ?>

                            <!-- next button -->
                            <?php
                            if ($pagination['next_page']) {
                                $nextPage = '?page=' . $pagination['next_page'];
                                $disableNextPageBtn = false;
                            } else {
                                $nextPage = 'javascript:void(0)';
                                $disableNextPageBtn = true;
                            }
                            ?>
                            <a href="<?php echo $nextPage; ?>" class="<?php if ($disableNextPageBtn) {
                                echo 'disabled';
                            } ?>" style="line-height: 20px;">
                                <i class="fas fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
    <!-- Feedback Modal -->
    <div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="feedbackModalTitle">Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="alert alert-danger hide error"></div>
                        <div class="wrap-input">
                            <div class="form-group required">
                                <label for="fullname" class="control-label"><small>Full name</small></label>
                                <input type="text" id="fullname" class="form-control" placeholder="Full name" required>
                            </div>
                            <div class="form-group required">
                                <label for="message" class="control-label"><small>Message (Maximum 500 characters)</small></label>
                                <textarea id="message" class="form-control" placeholder="Message" required rows="3"></textarea>
                            </div>
                        </div>
                        <div class="hide success-submit">
                            <div class="alert alert-success">
                                Thank you for your message. Please refresh the page to see newest messages.
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="action-before-submit">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <div class="action-submitted hide">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success refresh-page">Refresh page</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Feedback Modal Edit -->
    <div class="modal fade" id="feedbackModalEdit" tabindex="-1" role="dialog" aria-labelledby="feedbackModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="alert alert-danger hide error"></div>
                        <input type="hidden" id="edit_id">
                        <div class="form-group required">
                            <label for="fullname_edit" class="control-label"><small>Full name</small></label>
                            <input type="text" id="fullname_edit" class="form-control" placeholder="Full name" required>
                        </div>
                        <div class="form-group required">
                            <label for="message_edit" class="control-label"><small>Message (Maximum 500 characters)</small></label>
                            <textarea id="message_edit" class="form-control" placeholder="Message" required rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Confirm Delete Modal -->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="feedbackModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">
                        <input type="hidden" id="delete_id">
                        <p style="margin: 0;">Are you sure you want to delete this message?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="wrap-toast">
        <div class="edit-success">
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
                <div class="toast-header" style="display: none;">
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    <div class="alert alert-success">
                        Edit success
                    </div>
                </div>
            </div>
        </div>

        <div class="delete-success">
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
                <div class="toast-header" style="display: none;">
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    <div class="alert alert-success">
                        Message deleted
                    </div>
                </div>
            </div>
        </div>

        <div class="delete-failed">
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
                <div class="toast-header" style="display: none;">
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    <div class="alert alert-danger">
                        Something went wrong! Cannot delete message.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- built files will be auto injected -->
<script src="http://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="../../resources/js/guestbook.js"></script>
<script>
    if (window.sessionStorage.getItem('delete_success')) {
        $('.delete-success .toast').toast('show');
        window.sessionStorage.clear();
    }
</script>
</body>
</html>