<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Admin Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
</head>
<body class="login page">
<div id="app">
    <div class="container">
        <div class="page-content">
            <div class="content-wrapper">
                <div class="content">
                    <form action="/admin-login" method="post">
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div class="user-icon"><img src="../../resources/images/login-user.svg" alt="admin-login"></div>
                                <h5 class="content-group">
                                    Admin login
                                    <small class="display-block">
                                        Enter your credentials below
                                    </small>
                                </h5>
                            </div>
                            <?php
                                session_start();

                                if (isset($_SESSION['login_error'])) {
                                    ?>
                                    <div class="alert alert-danger alert-dismisable fade show">
                                        Invalid Credential
                                    </div>
                                    <?php
                                    session_destroy();
                                }
                            ?>
                            <!---->
                            <div class="form-group">
                                <label for="email"><small>Email</small></label>
                                <input id="email" name="email" type="email" class="form-control" autocomplete="off" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label for="password"><small>Password</small></label>
                                <input id="password" name="password" type="password" class="form-control" autocomplete="off" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-block" style="margin-top: 25px;">
                                    <span>Sign in</span>
                                </button>
                            </div>
                            <div class="text-center"><a href="/">Back to guest book</a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- built files will be auto injected -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>

</html>