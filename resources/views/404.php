<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
</head>

<body class="login page">
<div id="app">
    <div class="container">
        <div class="page-content">
            <div class="content-wrapper">
                <h4>Page not found</h4>
            </div>
        </div>
    </div>
</div>
<!-- built files will be auto injected -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>