-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: db
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (2,'admin@test.com','$2y$10$tTXYjFIHN8x6zdKuDy50POse7nUxsHlNcN3GQQpnWn2HkCHxml3lG',NULL,NULL);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guestbook`
--

DROP TABLE IF EXISTS `guestbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guestbook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `guestbook_updated_by_foreign` (`updated_by`),
  CONSTRAINT `guestbook_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admin` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guestbook`
--

LOCK TABLES `guestbook` WRITE;
/*!40000 ALTER TABLE `guestbook` DISABLE KEYS */;
INSERT INTO `guestbook` VALUES (1,2,'Thang Nguyen 1','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 12:53:22'),(2,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(3,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(4,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(5,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(6,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(7,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(8,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(9,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(10,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(11,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01'),(12,NULL,'Thang Nguyen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non libero vel quam semper hendrerit. Phasellus aliquet vitae lectus in fringilla. Praesent tincidunt convallis ultrices. Etiam ultricies finibus lacus, non sollicitudin justo ultricies vitae. Duis viverra ipsum in consequat aliquet. Morbi hendrerit tincidunt ullamcorper. Aliquam erat volutpat.','2019-03-31 10:57:01','2019-03-31 10:57:01');
/*!40000 ALTER TABLE `guestbook` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-31 20:08:04
