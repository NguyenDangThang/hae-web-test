<?php

return [
    "CREATE TABLE IF NOT EXISTS `admin` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `created_at` timestamp NULL DEFAULT NULL,
        `updated_at` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `admins_email_unique` (`email`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci",
    "CREATE TABLE IF NOT EXISTS `guestbook` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `updated_by` int(10) unsigned DEFAULT NULL,
        `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
        `created_at` timestamp NULL DEFAULT NULL,
        `updated_at` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `guestbook_updated_by_foreign` (`updated_by`),
        CONSTRAINT `guestbook_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admin` (`id`) ON DELETE SET NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
];