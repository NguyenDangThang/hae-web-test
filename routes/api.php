<?php

use Http\Route;

Route::post('api/message', 'Api\GuestbookController@store');

Route::put('api/message/{id}', 'Api\GuestbookController@update', 'AdminMiddleware');
Route::delete('api/message/{id}', 'Api\GuestbookController@destroy', 'AdminMiddleware');