<?php

use Http\Route;

Route::get('/', 'GuestbookController@index');
Route::get('/admin-login', 'LoginController@show', 'GuestMiddleware');
Route::post('/admin-login', 'LoginController@login', 'GuestMiddleware');
Route::get('/logout', 'LoginController@logout');