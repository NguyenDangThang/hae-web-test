## Project description
This is a simple project with purpose is complete the test for Web Developer position of HAE group.
## Project Admin credential:
- Username: `admin@test.com`
- Password: `123456`
## Installation
The project can run on Nginx and Apache but I prefer to use nginx (This project not tested with Apache yet, it may need to have a .htaccess file to make it work with Apache).
### Environment required
- Nginx server
- PHP version 7 or above (php7-fpm)
- MySQL version 5.7 or above
You need to install php-mysql extension to make sure the php mysql driver is valid when execute database query with `PDO` class.
You can follow the link (https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04) to setup the requirement above if you have any trouble to install those tools.
Nginx will use fast cgi to let php fpm to handle php script.

A tip to simplify the way you can test this project is you can skip install Nginx. PHP allow us to create a simple web server by run this command:
`php -S localhost:3000`. You still need to install PHP and MySQL, also php-mysql extension.
### Setup database
The configuration of database is locate on `config/database.php`. Please update the config with your environment.

There are 2 ways to setup tables of the project:

###### Use setup script
- Run `./setup` to create all required tables. Make sure that `setup` is executable. You can make it executable by run this command: `chmod +x setup`.
- Run `./generate-fake-data` to generate fake data, it will create 12 record of table `guestbook`. (You need to make sure this script is executable similar to `setup` script).
- Refresh database: run script `./reset-app-data`.

###### Import from sql file
You can import tables directly with sql file. The file is `data.sql`.


## Project Workflow

The idea of the project is all request will go through `index.php` file. In this file an application instance will be create. Application will map request to right route base on request method and request uri.

#### Routing
Check folder `routes`. There have api routes and web routes. Web routes will contain all route that can be render on browser. Api route is all Rest API.
A route will call to method `GET`, `POST`, `DELETE` and will have 3 params: `uri`, `controller and method map`, `middleware`

#### Controller
Route will map with a method in a Controller. Check all controllers at `app/Controllers`. Controller will render a page by return a `view` (for `web` route), or will return json data for `api` route.

#### Middleware
Before a request can handle by Controller, it need to check on middleware to make sure the request is valid before it executed by Controller.

## Security
- Prevent XSS by strip all tags when a visitor submit a message.
- Prevent SQL injection by use `PDO prepare statement`

## Project result
The project is satisfy the requirements:
- Create a guestbook to let visitors of a website submit messages. The messages must be visible on the
  site to everyone.
- The sites admin user should be able to delete and edit the messages.
- The message should be submitted using AJAX rather than standard form submission.
- The messages should be stored in an SQL database.
- The HTML output should be valid HTML5 and responsive for different screen sizes.

Project also come up with `Error handle` on frontend.
To make sure no problem cause `Cookie conflict`, please run the app on `browser incognito` mode.
## Conclusion
- This project is simple project that use VC in MVC pattern. We skip Model in this pattern and only use View and Controller.
- This project implement class loader by require all need files in `autoload.php`. In real life project, we use `Composer` instead to do this part.