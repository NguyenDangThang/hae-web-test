<?php

namespace app\Controllers;

use Database\DB;
use Http\Request;
use Support\Session;
use Support\View;

class LoginController
{
    public function show()
    {
        return View::make('admin-login');
    }

    public function login()
    {
        $request = new Request();

        $email = $request->get('email');
        $password = $request->get('password');

        $pdo = DB::pdo()->prepare("SELECT * from admin WHERE email = ?");
        $pdo->execute([$email]);
        $admin = $pdo->fetch(\PDO::FETCH_ASSOC);

        // refresh session
        session_destroy();
        // start session
        session_start();

        // check if credential valid
        if ($admin) {
            if (static::verifyPassword($password, $admin['password'])) {
                $_SESSION['logged_in'] = true;
                $_SESSION['logged_in_id'] = $admin['id'];
                $_SESSION['email'] = $admin['email'];
                return header("Location: /");
            }
        }

        // credential not valid
        $_SESSION['login_error'] = 'Invalid Credential';
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }

    public function logout()
    {
        session_start();
        $_SESSION = [];
        session_destroy();

        header('Location: /');
    }

    protected static function verifyPassword(string $rawPassword, string $storedPassword): bool
    {
        return password_verify($rawPassword, $storedPassword);
    }
}