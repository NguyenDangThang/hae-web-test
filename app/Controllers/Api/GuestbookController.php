<?php

namespace app\Controllers\Api;

use Database\DB;
use Http\Request;
use Http\Response;
use Support\Session;

class GuestbookController
{
    public function store()
    {
        $request = new Request();
        // get request params
        $full_name = $request->get('full_name');
        $message = $request->get('message');

        // validate data
        $dataErrors = static::validateGuestBookData($full_name, $message);

        if (count($dataErrors) > 0) {
            return Response::json(['message' => $dataErrors], 400);
        }

        // prevent xss
        static::sanitizeGuestBookData($full_name, $message);
        // store data
        $currentDatetime = date('Y-m-d H:i:s');
        $data = [
            'full_name' => $full_name,
            'message' => $message,
            'created_at' => $currentDatetime,
            'updated_at' => $currentDatetime,
        ];

        $insert = (new DB)->table('guestbook')->insert($data);

        if (!$insert) {
            return Response::json(['message' => 'Something went wrong'], 500);
        }

        // data created
        return Response::json($data, 201);
    }

    /**
     * Update a message in guestbook
     * Remember that only admin can update message
     *
     * @param $id
     */
    public function update($id)
    {
        // get current logged in user
        $admin = Session::user();

        $request = new Request();
        // get request params
        $full_name = $request->get('full_name');
        $message = $request->get('message');

        // validate data
        $dataErrors = static::validateGuestBookData($full_name, $message);

        if (count($dataErrors) > 0) {
            return Response::json(['message' => $dataErrors], 400);
        }

        // prevent xss
        static::sanitizeGuestBookData($full_name, $message);
        // store data
        $currentDatetime = date('Y-m-d H:i:s');
        $data = [
            'full_name' => $full_name,
            'message' => $message,
            'updated_at' => $currentDatetime,
            'updated_by' => $admin['id'],
        ];

        $update = (new DB)->table('guestbook')->update($data, [
            'id' => $id,
        ]);

        if (!$update) {
            return Response::json(['message' => 'Something went wrong'], 500);
        }

        // data created
        return Response::json($data, 200);
    }

    public function destroy($id)
    {
        $destroy = (new DB)->table('guestbook')->destroy(['id' => $id]);

        if (!$destroy) {
            return Response::json(['message' => 'Something went wrong'], 500);
        }

        // data created
        return Response::json(['message' => 'Deleted'], 200);
    }

    /**
     * Sanitize data to prevent XSS attach
     *
     * @param string $full_name
     * @param string $message
     */
    protected static function sanitizeGuestBookData(string &$full_name, string &$message)
    {
        $full_name = strip_tags(urldecode($full_name));
        $message = strip_tags(urldecode($message));
    }

    protected static function validateGuestBookData(
        string $full_name = null,
        string $message = null
    ) {
        // validate data
        $dataErrors = [];

        // full name is required
        if (!isset($full_name)) {
            $dataErrors[] = 'Full name is required';
        }

        // message is required
        if (!isset($message)) {
            $dataErrors[] = 'Message is required';
        }

        // message maximum character must smaller than 500
        if (strlen($message) > 500) {
            $dataErrors[] = 'Message only allow maximum 500 characters';
        }

        return $dataErrors;
    }
}