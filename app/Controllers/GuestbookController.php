<?php

namespace app\Controllers;

use Http\Request;
use Support\Session;
use Support\View;
use Database\DB;

class GuestbookController
{
    public function index()
    {
        $perPage = 6;
        // load messages
        // get current page
        $request = new Request();
        $currentPage = $request->get('page');

        if (!$currentPage) {
            $currentPage = 1;
        }

        // default load 6 per page;
        $offset = ($currentPage - 1) * $perPage;
        $pdo = DB::pdo();
        $stmp = $pdo->prepare("SELECT * from guestbook ORDER BY created_at DESC LIMIT $perPage OFFSET $offset");
        $stmp->execute();
        $messages = $stmp->fetchAll();

        if (!$messages) {
            $messages = [];
        }

        // get pagination meta
        $stmp = $pdo->prepare("SELECT COUNT(*) as total from guestbook");
        $stmp->execute();
        $totalItem = $stmp->fetch(\PDO::FETCH_ASSOC);
        $totalPage = ceil((intval($totalItem['total']) / $perPage));

        if ($currentPage > $totalPage) {
            header('Location: /?page=' . $totalPage);
        }

        $pagination = [
            'current_page' => $currentPage,
            'total_page' => $totalPage,
            'previous_page' => $currentPage > 1 ? $currentPage - 1 : null,
            'next_page' => $currentPage < $totalPage ? $currentPage + 1 : null,
        ];

        $loggedUser = Session::user();

        return View::make('guestbook', compact('messages', 'loggedUser', 'pagination'));
    }
}