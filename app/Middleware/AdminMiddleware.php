<?php

namespace app\Middleware;

use Http\Request;
use Http\Response;
use Support\Session;

class AdminMiddleware
{
    public function handle()
    {
        $request = new Request();
        // check if not logged in
        if (!Session::isLoggedIn()) {
            // check if request is api or not
            // if not then redirect back to login page
            // if is json then response error 401

            $requestUri = $request->getRequestUri();

            if (strpos($requestUri, 'api') !== false) {
                Response::json(['message' => 'Unauthenticated'], 401);
                die();
            } else {
                header('Location: /admin-login');
            }
        }
    }
}