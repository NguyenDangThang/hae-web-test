<?php

namespace app\Middleware;

use Support\Session;

class GuestMiddleware
{
    public function handle()
    {
        // check if logged in
        // if logged in then redirect to '/'
        if (Session::isLoggedIn()) {
            header('Location: /');
        }
    }
}